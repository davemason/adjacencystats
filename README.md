# AdjacencyStats

Scripts and code related to the calculation of adjacency statistics. Based on the work of [Hamilton _et al_](https://doi.org/10.1186/1471-2105-8-110).

## Matlab implementation

Matlab code to calculate adjacency stats originally written by [Brian Heit](http://phagocytes.ca/).

## IJM implementation

ImageJ macro code to calculate adjacency stats. Note that at this point, for proof of principle, thresholding is performed using Otsu's method (Hamilton uses 8-bit images and a threshold "Average intensity of pixels above 30 +/- 30.)