//@ File (label = "Input directory", style = "directory") inPath
//@ String (label = "File suffix", value = ".tif") suffix
//@ Boolean (label = "Debug mode?") debugMode

close("*");
tick=getTime();
if (debugMode==false){setBatchMode(true);} //-- runs in about 30% of time
print("-------------------------------------");print("Processing: "+inPath);
processFolder(inPath);
tock=getTime();
print("Finished in: "+((tock-tick)/1000)+" seconds");print("-------------------------------------");




//------ FUNCTIONS ------------

//-- function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(endsWith(list[i], suffix))
			processFile(input, list[i]);
	}
}


function processFile(input, file) {
open(input+File.separator+file);

title=getTitle();
getDimensions(width, height, channels, slices, frames);
if (debugMode==true){print("Processing: "+input);}
//print("Dimensions: "+width+" x "+height+" pixels");

//-- Threshold
setAutoThreshold("Otsu dark no-reset");
setOption("BlackBackground", true);
run("Convert to Mask");

//-- Calculate Adjacency

//-- number of white pixels
List.clear();
List.setMeasurements;
numWhitePixels=List.getValue("RawIntDen")/255;
classCount=newArray(0,0,0,0,0,0,0,0,0);

//--iterate pixels and count each class
for (r = 0; r < width; r++) {
	for (c = 0; c < height; c++) {
		if (getPixel(r, c)==255){
			//print("Pixel ["+r+"/"+c+"] is white");

			//-- Calculate the adjacency here
			makeRectangle(r-1,c-1,3,3);
			List.clear();
			List.setMeasurements;
			whiteAdj=List.getValue("RawIntDen")/255;
			whiteAdj=whiteAdj-1; //-- don't count the centre
			classCount[whiteAdj]++;
		}

	}
}

//-- Normalise the array based on the total number of white pixels and record to results
row=nResults;
setResult("filename", row, title);
classFraction=newArray(9);
for (i=0;i<classCount.length;i++){
	classFraction[i]=classCount[i]/numWhitePixels;
	//-- Record to results table
	setResult(i, row, d2s(classFraction[i],3));
}

} //-- close processFile function